create table seats
(
    uid INTEGER PRIMARY KEY,
    section INTEGER,
    row_letter TEXT,
    seat_number INTEGER
);

insert into seats values (null, 100, "A", 1);
insert into seats values (null, 100, "A", 2);
insert into seats values (null, 100, "A", 3);
insert into seats values (null, 100, "A", 4);
insert into seats values (null, 100, "A", 5);

insert into seats values (null, 100, "B", 1);
insert into seats values (null, 100, "B", 2);
insert into seats values (null, 100, "B", 3);
insert into seats values (null, 100, "B", 4);
insert into seats values (null, 100, "B", 5);

insert into seats values (null, 101, "A", 1);
insert into seats values (null, 101, "A", 2);
insert into seats values (null, 101, "A", 3);
insert into seats values (null, 101, "A", 4);
insert into seats values (null, 101, "A", 5);

insert into seats values (null, 101, "B", 1);
insert into seats values (null, 101, "B", 2);
insert into seats values (null, 101, "B", 3);
insert into seats values (null, 101, "B", 4);
insert into seats values (null, 101, "B", 5);

create table home_games
(
    uid INTEGER PRIMARY KEY,
    date TEXT,
    start_time TEXT,
    opponent TEXT
);

insert into home_games values (null, "2017-04-10", "15:10:00", "A's");
insert into home_games values (null, "2017-04-12", "19:15:00", "A's");
insert into home_games values (null, "2017-04-13", "19:15:00", "A's");
insert into home_games values (null, "2017-04-14", "19:15:00", "Angels");
insert into home_games values (null, "2017-04-15", "18:15:00", "Angels");
insert into home_games values (null, "2017-04-16", "13:15:00", "Angels");
insert into home_games values (null, "2017-04-18", "19:15:00", "Giants");
insert into home_games values (null, "2017-04-19", "19:15:00", "Giants");

create table seat_event_types
(
    uid INTEGER PRIMARY KEY,
    name TEXT
);

insert into seat_event_types values (1, "Sit");
insert into seat_event_types values (2, "Stand");
insert into seat_event_types values (3, "Closed");
insert into seat_event_types values (4, "Sold");

create table seat_events
(
    seat_id INTEGER,
    game_id INTEGER,
    timestamp INTEGER,
    event_type INTEGER
);

insert into seat_events values (1, 1, 1000, 1);
insert into seat_events values (1, 1, 1010, 2);
insert into seat_events values (3, 1, 1020, 1);
insert into seat_events values (2, 1, 1025, 1);
insert into seat_events values (1, 1, 1035, 1);


